# Readme
This app was developed as a learning project for Android. It is developed in Android Studio 2.1<br>

### Prerequiests ###

* [Android Development Basics](https://developer.android.com/courses)
* [Android Studio](https://developer.android.com/studio/?gclid=Cj0KCQjws536BRDTARIsANeUZ5-Z7usRWLof1IFlbFKK1D--BTP4v4MQWEeQYGQ_a9Twje8W7MTzQNoaAsC0EALw_wcB&gclsrc=aw.ds)
* minSdkVersion 21 or later
* targetSdkVersion 29
* compileSdkVersion 29
* Uses Gradle 5.6.4 or later
* Uses Gradle Plugin 3.6.2

### How do I get My App ###

* You can use our platform and configuration engine to build and automate your app once you push your changes to this branch.
* Just put your Recipents E-Mails List , (In Separate Lines) in [RECEIPENTS.md](https://bitbucket.org/mivorsdev/idengager_automotive_android_sdk/src/ff564194fee8bfb7bfb4b079c87fbe2b8744a541/RECEIPENTS.md?at=release%2FAljomaih-Android-SDK) before your push , and you will get your app automatically sent to you reciepints.

### Contribution guidelines ###

* Use the following instructions to invoke a black box testing:
* Open Android Studio and click on AVD to create your android virtual device.
* Choose your Device.
* Choose API Level.
* Enter your device name
* Now your device is ready to run.
* Make sure that your AVD is connected and running by executing the following command in terminal: {adb devices}
* Download the [Training Resources](https://drive.google.com/drive/folders/1d_5eUsNqs5LkoryCLdcMg_-oCKp72D40?usp=sharing) to run Appium Test by dragging and dropping the APK into Android Emulator.
* Open Appium and start server.
* Start Inspector Session
* Enter Key Name: deviceName and its Value : emulator-5554 which its value is coming from adb devices command.
* Enter Key Name: platformName and its Value : android (For iOS Emulator Enter value: ios).
* Enter Key Name: appPackage and its Value: (Execute the following steps)
* Open terminal and execute this command: adb logcat > logfile.txt
* Open logfile.txt and Press CTRL+F to search for (for activity) 
* Copy Package Name and enter it into value field.
* Enter Key Name: appActivity and its Value : (Use Activity name from the previous point).
* Enter Key Name: noReset (Type: Boolean) and its Value : True.
* Click on (Start Session).
* Once the session is started , you can find the application was automatically started in you AVD , and also you can inspect elements in appium.
* Install [Intellij](https://www.jetbrains.com/idea/download/#section=windows) on you system , in order to start your testing script.
* Open IDE and create a new java project
* In project structure (src) Folder past the (lib) Folder from the [downloaded resources](https://drive.google.com/drive/folders/1d_5eUsNqs5LkoryCLdcMg_-oCKp72D40?usp=sharing).
* Select all Jars in your project and click on (Add as Library).
* Create your testing java class and paste this Code.
```
	Define your capabilities in order to start your testing:
	DesiredCapabilities caps = new DesiredCapabilities(); 
	caps.setCapability("device", "emulator-5554");
	caps.setCapability("os_version", "10.0");
	caps.setCapability("platformName", "Android");
	caps.setCapability("automationName", "Appium");
	caps.setCapability("app", "D:\\Quiz-app.apk");
```
* Click Run → Run “Appium” or click on Shift+F10 to start your testing and you will get your app launched and tested according to your script on the AVD.

### Discussion? ###

* If you have any question you can contact with us Via Slack using this application : [send slack message](https://bitbucket.org/mivorsdev/idengager_automotive_android_sdk/src/a17cf6fff6f3fcf37f972c91f955ac0fdb533d61/Slack-send-Message.exe?at=release%2FAljomaih-Android-SDK)



